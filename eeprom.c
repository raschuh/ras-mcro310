/**********************************************************************************
 *
 * eeprom.c
 *
 * Anthology of the TWI applications with Microchip MCP24LC256 EEPROM
 *
 * Robert Schuh
 * Created on 30 November 2017
 * Last Modified on ---
 *
 **********************************************************************************/

#include "armshoe.h"
#include "as_twi.h"

static uint32_t hash(unsigned char *str);

void eeprom_io_test() 
{
    char msg[_BUF_LEN_LONG] = {0};

    const uint8_t D_ADR = 0x50; // The device address for MCP24LC256

    snprintf(msg, _BUF_LEN_LONG, "\n\n\r\t----- Hardware TWI Driver Demo -----\
                                  \n\n\r\tConnect PA3 to SDA and PA4 to SCL on MCP24LC256.\
                                  \n\r\tPress any key to start. Press ESC to exit.\n\n\r");
    usart0_write(msg, strlen(msg));

    twi0_config_master();

    clear_us0_buffer();

    while (1) {
        if (g_us0_rx.count != 0) break;
    }

    {
        uint32_t random_address = 0;
        uint8_t rx_data = 0, tx_data = 0;

        snprintf(msg, _BUF_LEN_LONG, "\n\n\r----- Random Byte Write and Read -----\n\n\r");
        usart0_write(msg, strlen(msg));

        for (int i = 0; i < 20; i++) {
            random_address = rand() & 0x7FFF;
            tx_data = (uint8_t)(rand() & 0xFF);

            twi0_write_byte(D_ADR, random_address, 2, tx_data);
            delay(0xFFFFF);
            twi0_read_byte(D_ADR, random_address, 2, &rx_data);

            snprintf(msg, _BUF_LEN_LONG, "Address: %4x; Data Sent: %2x; Data Received: %2x\n\r", random_address, tx_data, rx_data);
            usart0_write(msg, strlen(msg));
        }   
    } 

    {
        uint32_t start_address = 0x4242;
        uint32_t data_size = 25;
        uint8_t tx_data[25] = {0};
        uint8_t rx_data[25] = {0};

        for (int i = 0; i < data_size; i++) {
            tx_data[i] = (uint8_t)(rand() & 0xFF);
        }

        twi0_write(D_ADR, start_address, 2, tx_data, data_size);
        delay(0xFFFF);
        twi0_read(D_ADR, start_address, 2, rx_data, data_size);

        snprintf(msg, _BUF_LEN_LONG, "\n\n\r----- Page Write and Read -----\n\n\r");
        usart0_write(msg, strlen(msg));

        for (int i = 0; i < data_size; i++) {
            snprintf(msg, _BUF_LEN_LONG, "Address: %4x; Data Sent: %2x; Data Received: %2x\n\r", start_address + i, tx_data[i], rx_data[i]);
            usart0_write(msg, strlen(msg));
        }
    }

    clear_us0_buffer();
    twi0_cleanup();
}


void eeprom_password() 
{
    char msg[_BUF_LEN_LONG] = {0};
    char user_entry[_BUF_LEN_SHORT] = {0};
    uint32_t stored_password_hash = 0;
    uint32_t entry_hash = 0;
    uint8_t is_password_stored = 0;

    // State Map for password input validation
    char state_map[3][128] = {0};

    for (int i = 'a'; i <= 'z'; i++) state_map[0][i] = 1;
    for (int i = 'A'; i <= 'Z'; i++) state_map[0][i] = 1;
    for (int i = '0'; i <= '9'; i++) state_map[0][i] = 1;
    for (int i = 'a'; i <= 'z'; i++) state_map[1][i] = 1;
    for (int i = 'A'; i <= 'Z'; i++) state_map[1][i] = 1;
    for (int i = '0'; i <= '9'; i++) state_map[1][i] = 1;
    state_map[1]['\r'] = 2;

    const uint8_t D_ADR = 0x50; // The device address for MCP24LC256
    const uint16_t PASSWORD_BASE_ADDR = 0x4242;

    snprintf(msg, _BUF_LEN_LONG, "\n\n\r ----- EEPROM Application: Password Storage ----- \n\n\r");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "User entered password will be compared to the stored password on EEPROM.\n\n\r");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "Connect PA3 to SDA and PA4 to SCL on MCP24LC256.\n\r");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "Press any key to start. Press ESC to exit.\n\n\r");
    usart0_write(msg, strlen(msg));

    twi0_config_master();

    g_us0_rx.count = 0;

    while (1) {
        if (g_us0_rx.count != 0) break;
    }

    {   
        uint8_t data;

        for (int i = 0; i < 4; i++) {
            twi0_read_byte(D_ADR, PASSWORD_BASE_ADDR + i, 2, &data);
            delay(0xFFFF);
            stored_password_hash += (data << (8 * (3 - i)));
        }

        twi0_read_byte(D_ADR, PASSWORD_BASE_ADDR + 4, 2, &is_password_stored);
        delay(0xFFFF);
    }

    clear_us0_buffer();

    if (is_password_stored != 1) {
        snprintf(msg, _BUF_LEN_LONG, "The stored password is not initialized yet. Please enter a new password.\n\r");
        usart0_write(msg, strlen(msg));
        
        while (1) {
            snprintf(msg, _BUF_LEN_LONG, "The password must be a combination of upper and lower case letters and numbers.\n\r");
            usart0_write(msg, strlen(msg));
            snprintf(msg, _BUF_LEN_LONG, "> ");
            usart0_write(msg, strlen(msg));
            usart0_get_input(user_entry, _BUF_LEN_SHORT - 1);
            clear_us0_buffer();
            snprintf(msg, _BUF_LEN_LONG, "\n\r");
            usart0_write(msg, strlen(msg));

            if (ascii_state_map_test(user_entry, strlen(user_entry), state_map)) break;
        }

        stored_password_hash = hash(user_entry);

        for (int i = 0; i < 4; i++) {
            twi0_write_byte(D_ADR, PASSWORD_BASE_ADDR + i, 2, (uint8_t)(stored_password_hash >> (8 * (3 - i))));
            delay(0xFFFF);
        }

        twi0_write_byte(D_ADR, PASSWORD_BASE_ADDR + 4, 2, 0x01);
    }

    while (1) {
        clear_us0_buffer();

        while (1) {
            snprintf(msg, _BUF_LEN_LONG, "Enter password > ");
            usart0_write(msg, strlen(msg));
            usart0_get_input(user_entry, _BUF_LEN_SHORT - 1);
            clear_us0_buffer();
            snprintf(msg, _BUF_LEN_LONG, "\n\r");
            usart0_write(msg, strlen(msg));

            if (ascii_state_map_test(user_entry, strlen(user_entry), state_map)) break;
        }

        entry_hash = hash(user_entry);
        
        if (entry_hash == stored_password_hash) {
            break;
        }
        else {
            snprintf(msg, _BUF_LEN_LONG, "Incorrect password. Try again.\n\r");
            usart0_write(msg, strlen(msg));
        }
    }
        
    snprintf(msg, _BUF_LEN_LONG, "Welcome to Section 31's Memory Omega\n\rPress ESC to exit or 'r' to reset and exit.\n\n\r");
    usart0_write(msg, strlen(msg));
    clear_us0_buffer();

    while (1) {
        if (g_us0_rx.count == 0) continue;

        if (g_us0_rx.buffer[g_us0_rx.count - 1] == 27) {
            break;
        }
        else if (g_us0_rx.buffer[g_us0_rx.count - 1] == 'r') {
            twi0_write_byte(D_ADR, PASSWORD_BASE_ADDR + 4, 2, 0x0);
            break;
        }

        clear_us0_buffer();
    }

    clear_us0_buffer();
    twi0_cleanup();
}


static uint32_t hash(unsigned char *str)
{
    uint32_t hash = 5381;
    int c;

    while (c = *str++) hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}