/**********************************************************************************
 *
 * main_demo.c
 *
 * A simple demo of a SAM3S4B project.
 *
 * Robert Schuh
 * Created on 6 October 2017
 * Last Modified on 27 November 2017
 *
 **********************************************************************************/

#include "armshoe.h"

T_APP_MODE g_app_mode = {
    .keypad = 0,
    .parallel_data = 0,
    .pio_msg = 0
};

int main() {
    // Main menu for the main loop
    char main_menu[] = "\n\r\t----- Main Menu -----\
                        \n\r\t b - blinky\
                        \n\r\t k - keypad demo\
                        \n\r\t a - keypad ascii to number\
                        \n\r\t p - simplex_parallel_comm\
                        \n\r\t d - ADC demo\
                        \n\r\t e - EEPROM I/O H/W TWI Test\
                        \n\r\t w - EEPROM App: Password Storage\
                        \n\r\t 1 - W10Q1) LED States\
                        \n\r\t 2 - W10Q2) Button Messages\
                        \n\r\t 3 - W10Q3) Varible LED Blinking\
                        \n\r";

    int last_us0_receive_count = 0;
    int main_menu_size = strlen(main_menu);
    char ch = 0;

    app_init();

    usart0_write(main_menu, main_menu_size);

    while (1) {
        // If nothing changed at USART RX buffer then nothing has happened, skip this iteration
        if (last_us0_receive_count == g_us0_rx.count) {
            continue;
        }

        // Update the buffer state comparator
        last_us0_receive_count = g_us0_rx.count;

        // If there is something in the USART0 RX buffer then process it
        if (g_us0_rx.count > 0) {
            // Read the last char in the buffer
            ch = g_us0_rx.buffer[g_us0_rx.count - 1];

            switch (ch) {   
            case 'b':
                blinky();
                break;

            case 'k':
                keypad_demo();
                break;

            case 'a':
                keypad_atoif();
                break;

            case 'p':
                simplex_parallel_comm();
                break;

            case 'd':
                adc_demo();
                break;

            case '1':
                led_states();
                break;

            case '2':
                pioa_button_msg();
                break;

            case '3':
                variable_blinking();
                break;

            case 'e':
                eeprom_io_test();
                break;

            case 'w':
                eeprom_password();
                break;

            default:
                drNo();
                break;
            }

            usart0_write(main_menu, main_menu_size);
        }
    }
}