/**********************************************************************************
 *
 * armshoe.h
 *
 * An easy to use framework for simple Atmel SAM3S4B based Olimex board projects
 *
 * Robert Schuh
 * Created on 6 October 2017
 * Last Modified on 21 November 2017
 *
 **********************************************************************************/

#ifndef RAS_ARMSHOE_H
#define RAS_ARMSHOE_H

#include <sam3s.h>

#include "as_globals.h"
#include "as_memory_mapping.h"
#include "as_pio.h"
#include "as_usart.h"
#include "as_keypad.h"
#include "as_functions.h"

#include <debugio.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#endif