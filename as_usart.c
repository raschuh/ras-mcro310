/**********************************************************************************
 *
 * as_usart.c
 *
 * Robert Schuh
 * Created on 6 October 2017
 * Last Modified on 11 October 2017
 *
 **********************************************************************************/

#include "armshoe.h"

int g_pdc_buffer_transferred = 0;

T_BUFFER g_us0_rx = {{0}, 0};

int usart0_config() {
    //g_pusart = USART0;
    PMC->PMC_PCER0 = 1 << ID_USART0; // Enable clock for the PIOA input for USART0

    // Enable USART0 peripherals
    PIOA->PIO_PDR = 3 << 5;           // Enable USART0 pins as peripherals
    PIOA->PIO_ABCDSR[1] &= ~(3 << 5); // Enable A peripherals for USART0
    PIOA->PIO_ABCDSR[0] &= ~(3 << 5); // Enable A peripherals for USART0

    // Reset and disable RX and TX
    USART0->US_CR = (1 << 2)    // US_CR_RSTRX
                    | (1 << 3)  // US_CR_RSTTX
                    | (1 << 5)  // US_CR_RXDIS
                    | (1 << 7); // US_CR_TXDIS

    // Configure mode
    // Mode: async, 8 data bit, non parity, 1 stop bit
    USART0->US_MR = (0 << 8) | (4 << 9) // No parity
                    | (3 << 6)          // 8 bits
                    | (1 << 19);        // oversampling

    // Configure baudrate
    USART0->US_BRGR = _CPU_CLK / 8 / 9600;

    // Enable USART0 interrupt
    NVIC_EnableIRQ(USART0_IRQn);

    // Enable USART0 priority - set it to highest priority
    NVIC_SetPriority(USART0_IRQn, 0);

    // Enable TX and interruptible RX
    USART0->US_IER = 1 << 0; // US_CSR_RXRDY

    USART0->US_CR = (1 << 4)    // US_CR_RXEN
                    | (1 << 6); // US_CR_TXEN
}

extern inline void clear_us0_buffer() {
    memset(g_us0_rx.buffer, 0, sizeof(g_us0_rx.buffer));
    g_us0_rx.count = 0;
}

void usart0_write(char *buf, int len) {
    for (int i = 0; i < len; i++) {
        while ((USART0->US_CSR & (1 << 1)) == 0); // US_CSR_TXRDY - wait until USART0 is ready to transmit next char
        USART0->US_THR = buf[i]; // Send data element to the TX holding register
    }
}

void usart0_get_input(char buf[], int len) {
    while (1) {
        if (g_us0_rx.count >= len) break;
        if (g_us0_rx.buffer[g_us0_rx.count - 1] == '\r') break;
    }

    for (int i = 0; i < g_us0_rx.count - 1; i++) {
        buf[i] = g_us0_rx.buffer[i];
    }

    buf[g_us0_rx.count - 1] = 0;
}