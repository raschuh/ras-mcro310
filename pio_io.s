/**********************************************************************************
 *
 * pio_io.s
 *
 * Robert Schuh
 * Created on 31 October 2017
 * Last Modified on ---
 *
 **********************************************************************************/

    .global pio_write_asm // External function access
    .code 16 // Thumb or 16 bit code
    .syntax unified
    .thumb_func // 16 bit function
    .equ PIO_OER_OFFSET, (0x10)
    .equ PIO_SODR_OFFSET, (0x30)
    .equ PIO_CODR_OFFSET, (0x34)

/************************************************************
 * pio_write_asm
 *
 * Assembly version of pio_writep C function
 *
 * Parameters:
 * r0 - piox base address
 * r1 - pin_mask
 * r2 - state_mask
 *
 ************************************************************/
pio_write_asm: str r1, [r0] // Enable PIO -- *(piox (r0) + PIO_PER_OFFSET (0x00)) = pin_mask (r1)  
               str r1, [r0, #PIO_OER_OFFSET] // Enable as output
               str r1, [r0, #PIO_SODR_OFFSET] // Set pins high
               eor r3, r1, r2
               str r3, [r0, #PIO_CODR_OFFSET] // Set toggled pins low

               bx lr