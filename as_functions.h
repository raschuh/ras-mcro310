/**********************************************************************************
 *
 * as_functions.h
 *
 * Declare all of your application specific functions here or you can include
 * your application specific header files here.
 *
 * Robert Schuh
 * Created on 6 October 2017
 * Last Modified on ---
 *
 **********************************************************************************/

#ifndef RAS_SAM3S4B_FUNCTIONS_H
#define RAS_SAM3S4B_FUNCTIONS_H

inline void app_init();

int return_to_main();

inline void delay(int timer);

void hello();

void drNo();

void blinky();

void boop();

char ascii_state_map_test(char buf[], int buf_len, char map[][128]);

void hello_assembly();

char adc_demo();

void keypad_demo();

void keypad_atoif();

void simplex_parallel_comm();

char led_states();

char pioa_button_msg();

char variable_blinking();

void eeprom_io_test();

void eeprom_password();

#endif