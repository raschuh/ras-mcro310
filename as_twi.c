/**********************************************************************************
 *
 * as_twi.c
 *
 * TWI Driver
 *
 * Robert Schuh
 * Created on 30 November 2017
 * Last Modified on 6 December 2017
 *
 **********************************************************************************/

#include "as_twi.h"

void twi0_config_master() 
{
    // Multiplex to TWI0 as the active peripheral
    PIOA->PIO_ABCDSR[0] &= ~(TWD0_MASK);
    PIOA->PIO_ABCDSR[1] &= ~(TWD0_MASK);
    PIOA->PIO_ABCDSR[0] &= ~(TWCK0_MASK);
    PIOA->PIO_ABCDSR[1] &= ~(TWCK0_MASK);

    // Release the PIOA pins to TWI0
    PIOA->PIO_PDR = TWD0_MASK | TWCK0_MASK;

    // Enable PMC for TWI0
    PMC->PMC_PCER0 = (1 << ID_TWI0);

    // Reset TW10
    TWI0->TWI_CR = TWI_CR_SWRST;

    // Disable all TWI interrupts
    TWI0->TWI_IDR = 0xFFFF;

    // Set to master mode
    TWI0->TWI_CR = TWI_CR_MSEN | TWI_CR_SVDIS;

    // Configure TWI clock
    TWI0->TWI_CWGR = TWI_CWGR_CHDIV(12) | TWI_CWGR_CLDIV(12) | TWI_CWGR_CKDIV(2);
}


void twi0_cleanup()
{
    // Return the PIOA pins to PIO
    PIOA->PIO_PER = TWD0_MASK | TWCK0_MASK;

    // Disable PMC
    PMC->PMC_PCDR0 = (1 << ID_TWI0);
}


uint8_t twi0_write_byte(
    uint8_t d_addr, 
    uint32_t i_addr, 
    uint8_t i_addr_size, 
    uint8_t data
) 
{
    // Check if the internal address size is valid (1 - 3)
    if (i_addr_size > 3) return 1;

    // Set device address and size of internal address
    TWI0->TWI_MMR = TWI_MMR_DADR(d_addr) | (i_addr_size << TWI_MMR_IADRSZ_Pos);

    // Set internal address
    TWI0->TWI_IADR = TWI_IADR_IADR(i_addr);

    // Set the control register (CR)
    TWI0->TWI_CR = TWI_CR_START | TWI_CR_STOP | TWI_CR_MSEN | TWI_CR_SVDIS;

    // Write the data byte to the transmit holding register
    TWI0->TWI_THR = data;

    // Wait until the transmission is completed
    while (!(TWI0->TWI_SR & TWI_SR_TXRDY));
    while (!(TWI0->TWI_SR & TWI_SR_TXCOMP));
}


uint8_t twi0_write(
    uint8_t    d_addr,
    uint32_t   i_addr, 
    uint8_t    i_addr_size, 
    uint8_t   *buffer, 
    uint32_t   buffer_size
)
{
    // Check if the internal address size is valid (1 - 3)
    if (i_addr_size > 3) return 1;

    // Set device address and size of internal address
    TWI0->TWI_MMR = TWI_MMR_DADR(d_addr) | (i_addr_size << TWI_MMR_IADRSZ_Pos);

    // Set internal address
    TWI0->TWI_IADR = TWI_IADR_IADR(i_addr);

    // Set the control register (CR)
    TWI0->TWI_CR = TWI_CR_START | TWI_CR_STOP | TWI_CR_MSEN | TWI_CR_SVDIS;

    for (int i = 0; i < buffer_size; i++) {
        // Write the data byte to the transmit holding register
        TWI0->TWI_THR = buffer[i];

        // Wait until the transmission is completed
        while (!(TWI0->TWI_SR & TWI_SR_TXRDY));
    }

    // Wait until the transmission is completed
    while (!(TWI0->TWI_SR & TWI_SR_TXCOMP));
}


uint8_t twi0_read_byte(
    uint8_t    d_addr, 
    uint32_t   i_addr, 
    uint8_t    i_addr_size, 
    uint8_t   *data
)
{
    // Check if the internal address size is valid (1 - 3)
    if (i_addr_size > 3) return 1;

    // Set device address and size of internal address
    TWI0->TWI_MMR = TWI_MMR_DADR(d_addr) | TWI_MMR_MREAD | (i_addr_size << TWI_MMR_IADRSZ_Pos);

    // Set internal address
    TWI0->TWI_IADR = TWI_IADR_IADR(i_addr);

    // Set the control register (CR)
    TWI0->TWI_CR = TWI_CR_START | TWI_CR_STOP | TWI_CR_MSEN | TWI_CR_SVDIS;

    // Wait until a byte has been transferred to the receiver holding register
    while (!(TWI0->TWI_SR & TWI_SR_RXRDY));

    // Return the byte read from the receiver holding register
    *data = TWI0->TWI_RHR;

    // Wait until the process is completed
    while (!(TWI0->TWI_SR & TWI_SR_TXCOMP));
}


uint8_t twi0_read(
    uint8_t    d_addr, 
    uint32_t   i_addr, 
    uint8_t    i_addr_size, 
    uint8_t   *buffer, 
    uint32_t   buffer_size
)
{
    // Check if the internal address size is valid (1 - 3)
    if (i_addr_size > 3) return 1;

    // Set device address and size of internal address
    TWI0->TWI_MMR = TWI_MMR_DADR(d_addr) | TWI_MMR_MREAD | (i_addr_size << TWI_MMR_IADRSZ_Pos);

    // Set internal address
    TWI0->TWI_IADR = TWI_IADR_IADR(i_addr);

    // Set the control register (CR)
    TWI0->TWI_CR = TWI_CR_START | TWI_CR_MSEN | TWI_CR_SVDIS;

    for (int i = 0; i < buffer_size - 1; i++) {
        // Wait until a byte has been transferred to the receiver holding register
        while (!(TWI0->TWI_SR & TWI_SR_RXRDY));

        // Return the byte read from the receiver holding register
        buffer[i] = TWI0->TWI_RHR;
    }

    TWI0->TWI_CR = TWI_CR_STOP;

    // Wait until the last byte has been transferred to the receiver holding register
    while (!(TWI0->TWI_SR & TWI_SR_RXRDY));

    // Transfer the last byte then complete
    buffer[buffer_size - 1] = TWI0->TWI_RHR;

    // Wait until the process is completed
    while (!(TWI0->TWI_SR & TWI_SR_TXCOMP));
}