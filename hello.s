.global hello_assembly // global function access
.code 16 // thumb or 16 bit code
.syntax unified 
.thumb_func // 16 bit function

.equ PIOA_BASE, (0x400E0E00)
.equ PIO_OER_OFFSET, (0x10)
.equ PIO_SODR_OFFSET, (0x30)
.equ PIO_CODR_OFFSET, (0x34)
.equ LED1_MASK, (1 << 17)
.equ LED2_MASK, (1 << 18)
.equ LEDS_MASK, (0x3 << 17)

hello_assembly: mov r0, #5
                mov r1, #7
                add r2, r0, r1
                mov r0, #27

                ldr r4, =PIOA_BASE // Pseudo instruction to load long constant into the register
                mov r3, #LED1_MASK // load r3 with LED1_MASK
                mov r5, #LED2_MASK // load r5 with LED2_MASK
                orr r3, r3, r5
                str r3, [r4] // enable LED pin as pio
                str r3, [r4,#PIO_OER_OFFSET] // enable LED pin as pio output
                mov r0, #5
l:              str r3, [r4,#PIO_CODR_OFFSET] // set LED pin low - LED on
                str r3, [r4,#PIO_SODR_OFFSET] // set LED pin high - LED off
                sub r0, #1
                //cbnz r0, l
                bx lr // goto label l