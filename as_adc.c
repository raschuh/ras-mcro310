/**********************************************************************************
 *
 * as_adc.c
 *
 * Robert Schuh
 * Created on 9 November 2017
 * Last Modified on ---
 *
 **********************************************************************************/

#include "armshoe.h"

char adc_demo() {
    clear_us0_buffer();

    int c = 0;
    char msg[_BUF_LEN_LONG] = {0};

    // Enable Shana clock
    PMC->PMC_PCER0 = 1 << ID_ADC;

    // Reset ADC controller
    ADC->ADC_CR = 1;

    // Configure ADC mode register for freerun and slow processing
    ADC->ADC_MR = (1 << 7) | (0xFF << 8);

    // Enable ADC channels 4 and 5
    ADC->ADC_CHER = (0x3 << 4);

    // Start ADC controller
    ADC->ADC_CR = 1 << 1;

    snprintf(msg, _BUF_LEN_LONG, "\n\n\r\tADC Demo - Channel 4 : Pot and Channel 5 : Thermistor\n\r\tPress any key to start.\n\n\r");
    usart0_write(msg, strlen(msg));

    clear_us0_buffer();

    while (1) {
        if (g_us0_rx.count != 0) break;
    }

    clear_us0_buffer();

    while (1) {
        c = g_us0_rx.buffer[g_us0_rx.count - 1];
        snprintf(msg, _BUF_LEN_LONG - 1, "Ch 4 = %x, Ch 5 = %x, Ch 4 Value = %f, Ch 5 Value = %f\n\r",
                 ADC->ADC_CDR[4], ADC->ADC_CDR[5], ADC->ADC_CDR[4] / 4095.0 * 3.3, ADC->ADC_CDR[5] / 4095.0 * 3.3);
        usart0_write(msg, strlen(msg));
        if (c == 27) break;
    }
}