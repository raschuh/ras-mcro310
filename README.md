### SAIT MCRO-310 Microprocessor Fundamentals Classwork

My classwork for the microcontroller course. We studied embedded programming in the ARM Cortex M3 enviroment using C. The board we used is [Olimex SAM3-P256](https://www.olimex.com/Products/ARM/Atmel/SAM3-P256/).

We covered:

* Memory mapping
* C and ARM assembly
* GPIO I/O
* USART
* SPI
* TWI/I2C
* Timing
* PWM
* ADC
* EEPROM
* Power Management
* Interrupts