/**********************************************************************************
 *
 * week10-assign.c
 *
 * Robert Schuh
 * Created on 8 November 2017
 * Last Modified on ---
 *
 **********************************************************************************/

#include "armshoe.h"

T_IN_MSG_MAP g_btn_msg_map[2] = {
                                    {B1_MASK, 0, "B1 is pressed.\n\r"},
                                    {B2_MASK, 0, "B2 is pressed.\n\r"}
                                };

char led_states() {
    char ch = 0;
    int i = 0;
    int last_us0_count = 0;

    typedef struct {
        char key;
        uint32_t pin_mask;
        uint32_t state_mask;
    } T_STATE_MAP;

    T_STATE_MAP map[4] = {
                             {'a', LED1_MASK | LED2_MASK, ((1 << LED1_PIN) | (1 << LED2_PIN))},
                             {'s', LED1_MASK | LED2_MASK, ((1 << LED1_PIN) | (0 << LED2_PIN))},
                             {'d', LED1_MASK | LED2_MASK, ((0 << LED1_PIN) | (1 << LED2_PIN))},
                             {'f', LED1_MASK | LED2_MASK, ((0 << LED1_PIN) | (0 << LED2_PIN))}
                         };

    char state_map[1][128] = {0};
    char state = 0;
    state_map[0]['a'] = 1;
    state_map[0]['s'] = 1;
    state_map[0]['d'] = 1;
    state_map[0]['f'] = 1;
    state_map[0][27] = 1;

    clear_us0_buffer();

    char msg[_BUF_LEN_LONG] = {0};

    snprintf(msg, _BUF_LEN_LONG, "\n\n\r\t----- CHAR TO LED STATE MAPPING -----");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "\n\n\r\tPress any of the following characters to activate a LED state.");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "\n\n\r\ta - LED1 = OFF  LED2 = OFF");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "\n\r\ts - LED1 = OFF  LED2 = ON");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "\n\r\td - LED1 = ON   LED2 = OFF");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "\n\r\tf - LED1 = ON   LED2 = ON\n\n\r");
    usart0_write(msg, strlen(msg));  
    snprintf(msg, _BUF_LEN_LONG, "\tPress ESC to exit.\n\n\r");
    usart0_write(msg, strlen(msg));

    pio_set_output(PIOA, LED1_MASK | LED2_MASK, 0, PIO_DEFAULT);

    while (1) {
        if (g_us0_rx.count == 0) continue;

        ch = g_us0_rx.buffer[g_us0_rx.count - 1];
        
        state = 0;
        state = state_map[state][ch];

        if (state == 0) {
            snprintf(msg, _BUF_LEN_LONG, "Invalid selection! Try again.\n\r");
            usart0_write(msg, strlen(msg));
            clear_us0_buffer();
            continue;
        }

        if (return_to_main()) break;

        snprintf(msg, _BUF_LEN_LONG, "'%c' pressed.\n\r", ch);
        usart0_write(msg, strlen(msg));

        for (i = 0; i < 4; i++) {
            if (ch == map[i].key) {
                pio_write(PIOA, map[i].pin_mask, map[i].state_mask);
            }
        }

        clear_us0_buffer();
    }
          
    pio_write(PIOA, LED1_MASK | LED2_MASK, LED1_MASK | LED2_MASK); // Turn off all LEDs
    clear_us0_buffer();
                                                                              
    return 0;
}


char pioa_button_msg() {
    char msg[_BUF_LEN_LONG] = {0};

    g_btn_msg_map[0].isr_flag = 0;
    g_btn_msg_map[1].isr_flag = 0;

    int i = 0;

    clear_us0_buffer();

    snprintf(msg, _BUF_LEN_LONG, "\n\n\r\t----- INPUT EVENT MESSAGING -----\n\n\r");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "\tPress either B1 or B2 on the board to trigger a message.\n\n\r");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "\tPress ESC to exit.\n\n\r");
    usart0_write(msg, strlen(msg));

    pio_enable_pmc_clk(ID_PIOA);
    pio_set_input(PIOA, B1_MASK | B2_MASK, PIO_DEBOUNCE);
    pio_set_debounce_filter(PIOA, B1_MASK | B2_MASK, 0xFF);
    NVIC_EnableIRQ(PIOA_IRQn);
    NVIC_SetPriority(PIOA_IRQn, 0);
    pio_configure_interrupt(PIOA, B1_MASK | B2_MASK, PIO_IT_AIME | PIO_IT_EDGE);
    pio_enable_interrupt(PIOA, B1_MASK | B2_MASK);
    
    g_app_mode.pio_msg = 1;

    while (1) {
        if (return_to_main()) break;

        for (i = 0; i < 2; i++) {
            if (g_btn_msg_map[i].isr_flag) {
                snprintf(msg, _BUF_LEN_LONG, g_btn_msg_map[i].msg);
                usart0_write(msg, strlen(msg));
                g_btn_msg_map[i].isr_flag = 0;
            }
        }
    }

    g_app_mode.pio_msg = 0;

    clear_us0_buffer();
    pio_disable_pmc_clk(ID_PIOA);
    pio_disable_interrupt(PIOA, B1_MASK | B2_MASK);
    NVIC_DisableIRQ(PIOA_IRQn);

    return 0;
}


char variable_blinking() {
    char msg[_BUF_LEN_LONG] = {0};

    const int BASE_BLINK_RATE = 0xFFFF;
    const int MAX_BLINK_RATE = 100;

    int i = 0;
    int var_rate = 1;
    int num = 0;
    int power = 1;
    uint32_t leds = LED1_MASK | LED2_MASK;

    char state = 0;
    char state_map[4][128] = {0};
    state_map[0]['0'] = 2;
    for (i = '1'; i <= '9'; i++) state_map[0][i] = 1;
    for (i = '0'; i <= '9'; i++) state_map[1][i] = 1;
    state_map[1]['\r'] = 3;
    state_map[2]['\r'] = 3;

    clear_us0_buffer();

    snprintf(msg, _BUF_LEN_LONG, "\n\n\r\t----- VARIABLE BLINKY -----\n\n\r");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "\tEnter a string of digits to form an integer only number\n\r");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "\tto control the blinking rate of the board LEDs.\n\n\r");
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "\tA number must be between 0 and %d.\n\n\r", MAX_BLINK_RATE);
    usart0_write(msg, strlen(msg));
    snprintf(msg, _BUF_LEN_LONG, "\tPress ESC to exit.\n\n\r");
    usart0_write(msg, strlen(msg));

    pio_set_output(PIOA, leds, 1, PIO_DEFAULT);

    while (1) {
        if (return_to_main()) break;

        pio_toggle(PIOA, leds);
        delay(BASE_BLINK_RATE * var_rate);

        if (g_us0_rx.count == 0) continue;

        if (g_us0_rx.buffer[g_us0_rx.count - 1] == '\r') {
            state = 0;
            for (i = 0; i < strlen(g_us0_rx.buffer); i++) {
                state = state_map[state][g_us0_rx.buffer[i]];

                if (state == 0) break;
            }

            snprintf(msg, _BUF_LEN_LONG, "You entered %s\n\r", g_us0_rx.buffer);
            usart0_write(msg, strlen(msg));

            if (state == 0) {
                snprintf(msg, _BUF_LEN_LONG, "The entry is not a number.\n\r");
                usart0_write(msg, strlen(msg));
                clear_us0_buffer();
                continue;
            }

            if (strlen(g_us0_rx.buffer) > 4) {
                snprintf(msg, _BUF_LEN_LONG, "The integer is too long.\n\r");
                usart0_write(msg, strlen(msg));
                clear_us0_buffer();
                continue;
            }
            
            num = 0;
            power = 1;
            for (i = g_us0_rx.count - 2; i >= 0; i--) {
                num += power * (g_us0_rx.buffer[i] - '0');
                power *= 10;
            }

            if (num > MAX_BLINK_RATE) {
                snprintf(msg, _BUF_LEN_LONG, "The rate must be 0 to %d.\n\r", MAX_BLINK_RATE);
                usart0_write(msg, strlen(msg));
                clear_us0_buffer();
                continue;
            }
            
            var_rate = num;
            clear_us0_buffer();
        }
    }

    clear_us0_buffer();

    return 0;
}