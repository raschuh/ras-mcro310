/**********************************************************************************
 *
 * keypad.h
 *
 * Keypad structures
 *
 * Robert Schuh
 * Created on 13 November 2017
 * Last Modified on ---
 *
 **********************************************************************************/

#ifndef RAS_SAM3S4B_KEYPAD_H
#define RAS_SAM3S4B_KEYPAD_H

#include "armshoe.h"

typedef struct {
    int pin;
    int row_col;
} T_R_C_MAP;

typedef struct {
    uint32_t  cols_mask;
    uint32_t  rows_mask;
    T_R_C_MAP cols_map[3];
    T_R_C_MAP rows_map[4];
    short     current_col;
    short     current_row;
    T_BUFFER  entry;
    char      keypad_map[4][3];
} T_KEYPAD;

extern T_KEYPAD g_keypad;

#endif