/**********************************************************************************
 *
 * as_usart.h
 *
 * Support functions to ease the programming of the USART hardware.
 *
 * Robert Schuh
 * Created on 6 October 2017
 * Last Modified on 11 October 2017
 *
 **********************************************************************************/

#ifndef RAS_SAM3S4B_USART_H
#define RAS_SAM3S4B_USART_H

/**
 * Configure USART0 for use. This function must be called first
 * before USART0 can be used.
 */
int usart0_config();

/**
 * Reset the USART0 buffer and its counter
 */
inline void clear_us0_buffer();

/**
 * Write a string to USART0
 *
 * buf - a string to be written
 * len - the length of the string
 */
void usart0_write(char *buf, int len);

void usart0_get_input(char *buf, int len);

#endif