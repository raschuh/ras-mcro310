/**********************************************************************************
 *
 * as_twi.h
 *
 * TWI Driver
 *
 * Robert Schuh
 * Created on 30 November 2017
 * Last Modified on 6 December 2017
 *
 **********************************************************************************/

#ifndef RAS_SAM3S4B_TWI_H
#define RAS_SAM3S4B_TWI_H

#include <sam3s.h>

#define TWD0_MASK   (PIO_PA3)
#define TWCK0_MASK  (PIO_PA4)
#define TWD1_MASK   (PIO_PB4)
#define TWCK1_MASK  (PIO_PB5)

void twi0_config_master();

void twi0_cleanup();

uint8_t twi0_write_byte(
    uint8_t d_addr, 
    uint32_t i_addr, 
    uint8_t i_addr_size, 
    uint8_t data
);

uint8_t twi0_write(
    uint8_t    d_addr,
    uint32_t   i_addr, 
    uint8_t    i_addr_size, 
    uint8_t   *buffer, 
    uint32_t   buffer_size
);

uint8_t twi0_read_byte(
    uint8_t    d_addr, 
    uint32_t   i_addr, 
    uint8_t    i_addr_size, 
    uint8_t   *data
);

uint8_t twi0_read(
    uint8_t    d_addr, 
    uint32_t   i_addr, 
    uint8_t    i_addr_size, 
    uint8_t   *buffer, 
    uint32_t   buffer_size
);

#endif