/**********************************************************************************
 *
 * simplex_parallel_comm.c
 *
 * Robert Schuh
 * Created on 23 October 2017
 * Last Modified on ---
 *
 **********************************************************************************/

#include "armshoe.h"

uint32_t g_par_data_in_mask = (1 << 0) | (1 << 1) | (1 << 2) | (1 << 3);
uint32_t g_par_data_out_mask = (1 << 7) | (1 << 8) | (1 << 9) | (1 << 10);
uint32_t g_par_in_clk_mask = (1 << 4);
uint32_t g_par_out_clk_mask = (1 << 15);
uint32_t g_par_rx_int_flag = 0;
uint32_t g_par_rx_data = 0;

T_BUFFER g_parallel_rx = {{0}, 0};

void clear_p_rx_buffer() {
    memset(g_parallel_rx.buffer, 0, sizeof(g_parallel_rx.buffer));
    g_parallel_rx.count = 0;
}


void simplex_parallel_comm() {
    clear_us0_buffer();

    char msg[_BUF_LEN_LONG] = {0};

    snprintf(msg, _BUF_LEN_LONG - 1, "\n\n\r\t\t------ Simplex Parallel Comm ------\n\n\r");
    usart0_write(msg, strlen(msg));

    PIOA->PIO_PER = g_par_data_in_mask | g_par_data_out_mask | g_par_in_clk_mask | g_par_out_clk_mask;
    PIOA->PIO_ODR = g_par_data_in_mask | g_par_in_clk_mask;
    PIOA->PIO_OER = g_par_data_out_mask | g_par_out_clk_mask;
    PIOA->PIO_CODR = g_par_out_clk_mask;

    pio_enable_pmc_clk(ID_PIOA);
    NVIC_EnableIRQ(PIOA_IRQn);
    NVIC_SetPriority(PIOA_IRQn, 0);
    pio_configure_interrupt(PIOA, g_par_in_clk_mask, PIO_IT_AIME | PIO_IT_EDGE | PIO_IT_RE_OR_HL);
    pio_enable_interrupt(PIOA, g_par_in_clk_mask);

    g_app_mode.parallel_data = 1;
    uint32_t tx_ch = 0;
    int last_usart_buf_count = 0;
    int nibble = 0, i = 0;  
    char rx_ch = 0;
    uint32_t st = 0;
                  
    while (1) {
        if (return_to_main()) break;

        if (last_usart_buf_count == g_us0_rx.count) continue;

        last_usart_buf_count = g_us0_rx.count;
        
        if (g_us0_rx.buffer[g_us0_rx.count - 1] == '\r') {
            for (i = 0; i < g_us0_rx.count - 1; i++) {
                for (nibble = 0; nibble < 2; nibble++) {
                    tx_ch = g_us0_rx.buffer[i];
                    tx_ch = tx_ch >> (nibble * 4);
                    tx_ch = (tx_ch & 0xF) << 7;
                    PIOA->PIO_SODR = tx_ch;
                    PIOA->PIO_CODR = g_par_data_out_mask ^ tx_ch;
                    PIOA->PIO_SODR = g_par_out_clk_mask;
                }
            }

            for (i = 0; i < g_parallel_rx.count; i += 2) {
                rx_ch = g_parallel_rx.buffer[i] | ((g_parallel_rx.buffer[i + 1]) << 4);
                sprintf(msg, "Character received: %c\n\r", rx_ch);
                usart0_write(msg, strlen(msg));
            }

            clear_p_rx_buffer();
            clear_us0_buffer();
        }
    }

    g_app_mode.parallel_data = 0;
    clear_us0_buffer();
    pio_disable_interrupt(PIOA, g_par_in_clk_mask);
    NVIC_DisableIRQ(PIOA_IRQn);
    PIOA->PIO_PDR = g_par_data_in_mask | g_par_data_out_mask | g_par_in_clk_mask | g_par_out_clk_mask;
    pio_disable_pmc_clk(ID_PIOA);

    return;
}