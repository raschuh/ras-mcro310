/**********************************************************************************
 *
 * as_ihandlers.c
 *
 * Implementation of certain hardware (NVIC) interrupt handlers as defined by 
 * Atmel. The declaration of all NVIC handlers can be found in the SAM3S4B header 
 * file written by Atmel.
 *
 * All interrupt handler implementations should go in this file.
 *
 * Robert Schuh
 * Created on 6 October 2017
 * Last Modified on 12 October 2017
 *
 **********************************************************************************/

#include "armshoe.h"

void USART0_Handler() {
    // Get USART0 channel status - check if RX is ready and TX has ended
    uint32_t status = USART0->US_CSR & ((1 << 0)     // US_CSR_RXRDY
                                        | (1 << 4)); // US_CSR_ENDTX

    // If USART0 RX is ready to receive then read the characters into the buffer
    if (status & (1 << 0)) {
        g_us0_rx.buffer[g_us0_rx.count++] = USART0->US_RHR;
    } else if (status & (1 << 4)) {   // US_CSR_ENDTX
        g_pdc_buffer_transferred = 1; // PDC buffer is empty
    }

    // Reset the buffer and set the counter to 0 if the buffer is overflowing
    if (g_us0_rx.count >= _BUF_LEN_LONG) {
        clear_us0_buffer();
    }
}

void PIOA_Handler() {
    volatile uint32_t isr = PIOA->PIO_ISR;

    // ISR for the Keypad activated by an application routine
    if (g_app_mode.keypad) {
        // Decode which column pin is triggered then get its keypad column value
        uint32_t col_pin_mask = isr & g_keypad.cols_mask;

        for (int i = 0; i < 3; i++) {
            if (col_pin_mask == (1 << g_keypad.cols_map[i].pin)) {
                g_keypad.current_col = g_keypad.cols_map[i].row_col;
                break;
            }
        }

        // NOTE: The IO operations below can be performed by calling pio_read and pio_write
        // functions, however since this is an ISR so the code must be fast and compact as
        // possible. Also, functions calls from an ISR can cause issues that can be difficult
        // to debug.

        // Now, set the column pin as output and drive it high so it can source current to
        // the row being shorted by the button press
        PIOA->PIO_OER = col_pin_mask;
        PIOA->PIO_SODR = col_pin_mask;

        // Set the rows as input and read their state to identify which row has received the
        // current from the triggered column pin
        PMC->PMC_PCER0 = 1 << ID_PIOA;
        PIOA->PIO_ODR = g_keypad.rows_mask;
        uint32_t row_pin_mask = PIOA->PIO_PDSR & g_keypad.rows_mask;
        
        for (int i = 0; i < 4; i++) {
            if (row_pin_mask == (1 << g_keypad.rows_map[i].pin)) {
                g_keypad.current_row = g_keypad.rows_map[i].row_col;
                break;
            }
        }

        g_keypad.entry.buffer[g_keypad.entry.count++] = g_keypad.keypad_map[g_keypad.current_row][g_keypad.current_col];

        if (g_keypad.entry.count >= _BUF_LEN_LONG) {
            g_keypad.entry.count = 0;
            memset(g_keypad.entry.buffer, 0, sizeof(g_keypad.entry.buffer));
        }

        // Set the pins to its default state
        PIOA->PIO_ODR = g_keypad.cols_mask;
        PIOA->PIO_OER = g_keypad.rows_mask;
        PIOA->PIO_CODR = g_keypad.rows_mask;

        return;
    }
    else if (g_app_mode.parallel_data) {
        if ((isr & g_par_in_clk_mask) == 0) return;

        g_parallel_rx.buffer[g_parallel_rx.count++] = ((PIOA->PIO_PDSR) & g_par_data_in_mask);

        if (g_parallel_rx.count >= _BUF_LEN_LONG) {
            memset(g_parallel_rx.buffer, 0, sizeof(g_parallel_rx.buffer));
            g_parallel_rx.count = 0;
        }

        PIOA->PIO_CODR = g_par_out_clk_mask;
        return;
    }
    else if (g_app_mode.pio_msg) {
        if (isr & B1_MASK) {
            g_btn_msg_map[0].isr_flag = 1;
        }
        else if (isr & B2_MASK) {
            g_btn_msg_map[1].isr_flag = 1;
        }
    }
}

void PIOB_Handler() {
    // NOT IMPLEMENTED
}