/**********************************************************************************
 *
 * keypad.c
 *
 * Robert Schuh
 * Created on 11 October 2017
 * Last Modified on ---
 *
 **********************************************************************************/

#include "armshoe.h"

// NOTE: The keypad is configured to connect to PIOA pin 25 to 31 in order
// in respect to the keypad's pins. That is, the keypad's pin 1 to 7 are
// connected to PIOA pin 31 to 25 respectively.
T_KEYPAD g_keypad = {
    .cols_mask = 0,
    .rows_mask = 0,
    .cols_map = {{29, 0}, {25, 1}, {27, 2}},
    .rows_map = {{28, 0}, {30, 1}, {31, 2}, {26, 3}},
    .current_col = -1,
    .current_row = -1,
    .entry = {{0}, 0},
    .keypad_map = {
                    {'1', '2', '3'},
                    {'4', '5', '6'},
                    {'7', '8', '9'},
                    {'*', '0', '#'}
                  }
};

void init_keypad() {
    // ---- Initialize the PIOA for keypad use ----

    for (int i = 0; i < 3; i++) g_keypad.cols_mask |= 1 << g_keypad.cols_map[i].pin;
    for (int i = 0; i < 4; i++) g_keypad.rows_mask |= 1 << g_keypad.rows_map[i].pin;

    PIOA->PIO_PER = g_keypad.cols_mask | g_keypad.rows_mask;

    PIOA->PIO_PUER = g_keypad.cols_mask; // Pull up the column pins
    PIOA->PIO_PPDER = g_keypad.rows_mask; // Pull down the row pins

    // Set the pins to its default state
    PIOA->PIO_ODR = g_keypad.cols_mask;
    PIOA->PIO_OER = g_keypad.rows_mask;
    PIOA->PIO_CODR = g_keypad.rows_mask;
    
    pio_enable_pmc_clk(ID_PIOA);
    pio_set_debounce_filter(PIOA, g_keypad.cols_mask, 0xFF);
    PIOA->PIO_IFER = g_keypad.cols_mask;

    NVIC_EnableIRQ(PIOA_IRQn);
    NVIC_SetPriority(PIOA_IRQn, 0);
    pio_configure_interrupt(PIOA, g_keypad.cols_mask, PIO_IT_AIME | PIO_IT_EDGE);
    pio_enable_interrupt(PIOA, g_keypad.cols_mask);
}


void keypad_demo() {
    clear_us0_buffer();

    char msg[_BUF_LEN_LONG] = {0};

    snprintf(msg, _BUF_LEN_LONG, "\n\n\r\t ---- KEYPAD DEMO ----\n\n\r");
    usart0_write(msg, strlen(msg));

    init_keypad();

    g_app_mode.keypad = 1;

    while (1) {
        if (return_to_main()) {
            break;
        }

        if (g_keypad.entry.count == 6) {
            snprintf(msg, _BUF_LEN_LONG, "\n\r\t\tThe sequence entered is %s\n\r", g_keypad.entry.buffer);
            usart0_write(msg, strlen(msg));
            g_keypad.entry.count = 0;
            memset(g_keypad.entry.buffer, 0, sizeof(g_keypad.entry.buffer));
        }
    }

    // ---- Clean up the PIOA configuration for other uses ----
    PIOA->PIO_IDR = g_keypad.cols_mask;
    NVIC_DisableIRQ(PIOA_IRQn);
    
    g_app_mode.keypad = 0;

    return;
}


/*********************************************************************
 *
 * keypad_atoif
 *
 * Capture a decimal value from the keypad then validate the input
 * and convert to either double or unsigned integer.
 *
 *********************************************************************/
void keypad_atoif() {
    clear_us0_buffer();

    char msg[_BUF_LEN_LONG] = {0};    // General purpose buffer for USART0 output
    g_keypad.keypad_map[3][0] = '.';  // Adjust the default map for the keypad - * to .
    g_keypad.keypad_map[3][2] = '\r'; // Adjust the default map for the keypad - # to \r

    /*
       The state_map code below is the implementation of the following state table.
       There are three different cases to validate the keypad input:

       d[d...]\r for integers
       d[d...].\r for real
       [d...].d[d...]\r for real

           d   .   \r
        0  1   2    0   
        1  1   3    4
        2  3   0    0
        3  3   0    4
        4  
    */

    char state_map[5][128] = {0};
    int state = 0;

    for (int i = '0'; i <= '9'; i++) state_map[0][i] = 1;
    for (int i = '0'; i <= '9'; i++) state_map[1][i] = 1;
    for (int i = '0'; i <= '9'; i++) state_map[2][i] = 3;
    for (int i = '0'; i <= '9'; i++) state_map[3][i] = 3;
    state_map[0]['.'] = 2;
    state_map[1]['.'] = 3;
    state_map[1]['\r'] = 4;
    state_map[3]['\r'] = 4;

    double num_acc = 0; // The accumulator for real value, it can be casted to an integer if necessary 
    int float_flag = 0; // If the decimal point has been encountered then flag the input as real
    int power = 1;      // Current power multiplication
    
    snprintf(msg, _BUF_LEN_LONG, "\n\n\r\t ---- KEYPAD ASCII TO FLOAT/INT ----\n\n\r");
    usart0_write(msg, strlen(msg));

    init_keypad();

    g_app_mode.keypad = 1;

    int last_keypad_buffer_count = 0;

    while (1) {
        if (return_to_main()) {
            break;
        }

        if (last_keypad_buffer_count == g_keypad.entry.count) {
            continue;
        }

        last_keypad_buffer_count = g_keypad.entry.count;

        if (g_keypad.entry.buffer[g_keypad.entry.count - 1] == '\r') {
            // Validate the keypad input using the state machine table
//            state = 0;
//            for (int i = 0; i < strlen(g_keypad.entry.buffer); i++) {
//                state = state_map[state][g_keypad.entry.buffer[i]];
//
//                if (state == 0) {
//                    break;
//                }
//            }
//            
//            // If the input has failed the state machine test then reject the input and notify the user
//            if (state == 0) {
            if (ascii_state_map_test(g_keypad.entry.buffer, strlen(g_keypad.entry.buffer), state_map) == 0) {
                snprintf(msg, _BUF_LEN_LONG, "\n\r\t\tThe entry is invalid. Please enter again.\n\r");
                usart0_write(msg, strlen(msg));
                memset(g_keypad.entry.buffer, 0, sizeof(g_keypad.entry.buffer));
                g_keypad.entry.count = 0;
                continue;
            }

            num_acc = 0;
            power = 1;
            float_flag = 0;

            // If passed the state machine test then process the input
            for (int i = g_keypad.entry.count - 2; i >= 0; i--) {
                // If the decimal point has been encountered then raise the float flag and shift the value
                // to the right of the decimal point
                if (g_keypad.entry.buffer[i] == '.') {
                    float_flag = 1;
                    num_acc /= power;
                    power = 1;
                    continue;
                }

                num_acc += (g_keypad.entry.buffer[i] - '0') * power;
                power *= 10;
            }

            // If there is no decimal point then cast the accumulator to unsigned integer
            // NOTE: other approaches can be used such as if ((num_acc - (int)num_acc) == 0) but there is no
            // guarantee that it will always work due to rounding, conversion, and limiting errors. 
            if (float_flag == 0) {
                snprintf(msg, _BUF_LEN_LONG, "\n\r\t\tAn integer has been entered. The value is %d.\n\r", (unsigned int)num_acc);
            }
            else {
                snprintf(msg, _BUF_LEN_LONG, "\n\r\t\tA float has been entered. The value is %.5f.\n\r", num_acc);
            }

            usart0_write(msg, strlen(msg));

            memset(g_keypad.entry.buffer, 0, sizeof(g_keypad.entry.buffer));
            g_keypad.entry.count = 0;
        }
    }

    // ---- Clean up the PIOA configuration for other uses ----
    PIOA->PIO_IDR = g_keypad.cols_mask;
    NVIC_DisableIRQ(PIOA_IRQn);
    
    g_app_mode.keypad = 0;

    clear_us0_buffer();

    return;
}