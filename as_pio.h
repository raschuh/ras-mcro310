/**********************************************************************************
 *
 * as_pio.h
 *
 * Support functions to ease the programming of the PIO hardware.
 * 
 * Robert Schuh
 * Created on 6 October 2017
 * Last Modified on 22 November 2017
 *
 **********************************************************************************/

#ifndef RAS_SAM3S4B_PIO_H
#define RAS_SAM3S4B_PIO_H

#include "armshoe.h"

/*  Default pin configuration (no attribute). */
#define PIO_DEFAULT             (0u << 0)
/*  The internal pin pull-up is active. */
#define PIO_PULLUP              (1u << 0)
/*  The internal glitch filter is active. */
#define PIO_DEGLITCH            (1u << 1)
/*  The pin is open-drain. */
#define PIO_OPENDRAIN           (1u << 2)
/*  The internal debouncing filter is active. */
#define PIO_DEBOUNCE            (1u << 3)
/*  Enable additional interrupt modes. */
#define PIO_IT_AIME             (1u << 4)
/*  Interrupt High Level/Rising Edge detection is active. */
#define PIO_IT_RE_OR_HL         (1u << 5)
/*  Interrupt Edge detection is active. */
#define PIO_IT_EDGE             (1u << 6)
/*  The pin is sync'd - direct ODSR  */
#define PIO_SYNC                (1u << 7)
/*  Low level interrupt is active */
#define PIO_IT_LOW_LEVEL        (0               | 0 | PIO_IT_AIME)
/*  High level interrupt is active */
#define PIO_IT_HIGH_LEVEL       (PIO_IT_RE_OR_HL | 0 | PIO_IT_AIME)
/*  Falling edge interrupt is active */
#define PIO_IT_FALL_EDGE        (0               | PIO_IT_EDGE | PIO_IT_AIME)
/*  Rising edge interrupt is active */
#define PIO_IT_RISE_EDGE        (PIO_IT_RE_OR_HL | PIO_IT_EDGE | PIO_IT_AIME)

void pio_set_input(Pio *pio, const uint32_t mask, const uint32_t attributes);
void pio_set_output(Pio *pio, const uint32_t mask, const uint32_t default_level, const uint32_t attributes);
void pio_set_sync_output(Pio *pio, const uint32_t mask, const uint32_t sync_enable);
void pio_enable_pmc_clk(const uint32_t id);
void pio_disable_pmc_clk(const uint32_t id);
void pio_set_pull_up(Pio *pio, const uint32_t mask, const uint32_t pull_up_enable);
void pio_set_pull_down(Pio *pio, const uint32_t mask, const uint32_t pull_down_enable);
void pio_set_multi_driver(Pio *pio, const uint32_t mask, const uint32_t multi_driver_enable);
void pio_set_debounce_filter(Pio *pio, const uint32_t mask, const uint32_t cut_off);
void pio_write(Pio *pio, const uint32_t mask, const uint32_t level);
void pio_toggle(Pio *pio, const uint32_t mask);
void pio_sync_write(Pio *pio, const uint32_t mask);
void pio_sync_toggle(Pio *pio, const uint32_t mask);
uint32_t pio_read(Pio *pio, uint32_t mask);
void pio_enable_interrupt(Pio *pio, const uint32_t mask);
void pio_disable_interrupt(Pio *pio, const uint32_t mask);
void pio_configure_interrupt(Pio *pio, const uint32_t mask, const uint32_t attributes);

#endif