/**********************************************************************************
 *
 * as_pio.c
 *
 * Robert Schuh
 * Created on 6 October 2017
 * Last Modified on 22 November 2017
 *
 **********************************************************************************/

#include "armshoe.h"

#define FREQ_SLOW_CLOCK_EXT 32768

void pio_set_input(Pio *pio, const uint32_t mask, const uint32_t attributes) {
    pio_disable_interrupt(pio, mask);
    pio_set_pull_up(pio, mask, attributes & PIO_PULLUP);

    // Enable Input Filter if necessary
    if (attributes & (PIO_DEGLITCH | PIO_DEBOUNCE)) {
        pio->PIO_IFER = mask;
    }
    else {
        pio->PIO_IFDR = mask;
    }

    // Enable de-glitch or de-bounce if filter is enabled
    if (attributes & PIO_DEGLITCH) {
        pio->PIO_IFSCDR = mask;
    }
    else if (attributes & PIO_DEBOUNCE) {
        pio->PIO_IFSCER = mask;
    }

    // Configure pin as input
    pio->PIO_ODR = mask;
    pio->PIO_PER = mask;
}


void pio_set_output(Pio *pio, const uint32_t mask, const uint32_t default_level, const uint32_t attributes) {
    pio_disable_interrupt(pio, mask);
    pio_set_pull_up(pio, mask, attributes & PIO_PULLUP);

    // Enable synchronous data output if flag is set
    pio_set_sync_output(pio, mask, attributes & PIO_SYNC);

    // Enable multi-drive if necessary
    if (attributes & PIO_OPENDRAIN) {
        pio->PIO_MDER = mask;
    }
    else {
        pio->PIO_MDDR = mask;
    }

    // Set default logic level
    if (default_level) {
        pio->PIO_SODR = mask;
    }
    else {
        pio->PIO_CODR = mask;
    }

    pio->PIO_OER = mask;
    pio->PIO_PER = mask;
}


void pio_set_sync_output(Pio *pio, const uint32_t mask, const uint32_t sync_enable) {
    if (sync_enable) {
        pio->PIO_OWER = mask;
    }
    else {
        pio->PIO_OWDR = mask;
    }
}


void pio_enable_pmc_clk(const uint32_t id) {
    uint32_t id_mask = (1u << id);
    if ((PMC->PMC_PCSR0 & id_mask) != id_mask) {
        PMC->PMC_PCER0 = id_mask;
    }
}


void pio_disable_pmc_clk(const uint32_t id) {
    uint32_t id_mask = (1u << id);
    if ((PMC->PMC_PCSR0 & id_mask) == id_mask) {
        PMC->PMC_PCDR0 = id_mask;
    }
}


void pio_set_pull_up(Pio *pio, const uint32_t mask, const uint32_t pull_up_enable) {
    if (pull_up_enable) {
        pio->PIO_PUER = mask;
    }
    else {
        pio->PIO_PUDR = mask;
    }
}


void pio_set_pull_down(Pio *pio, const uint32_t mask, const uint32_t pull_down_enable) {
    if (pull_down_enable) {
        pio->PIO_PPDER = mask;
    }
    else {
        pio->PIO_PPDDR = mask;
    }
}


void pio_set_multi_driver(Pio *pio, const uint32_t mask, const uint32_t multi_driver_enable) {
    // Enable the multi-driver if necessary
	if (multi_driver_enable) {
		pio->PIO_MDER = mask;
	} else {
		pio->PIO_MDDR = mask;
	}
}


void pio_set_debounce_filter(Pio *pio, const uint32_t mask, const uint32_t cut_off) {
    pio->PIO_IFSCER = mask;

    /*
	 * The debouncing filter can filter a pulse of less than 1/2 Period of a
	 * programmable Divided Slow Clock:
	 * Tdiv_slclk = ((DIV+1)*2).Tslow_clock
	 */
    pio->PIO_SCDR = PIO_SCDR_DIV((FREQ_SLOW_CLOCK_EXT / (2 * cut_off)) - 1);
}


void pio_write(Pio *pio, const uint32_t mask, const uint32_t level) {
    pio->PIO_SODR = level;
    pio->PIO_CODR = mask ^ level;
}


void pio_toggle(Pio *pio, const uint32_t mask) {
    uint32_t old_level = pio->PIO_ODSR & mask;
    pio->PIO_CODR = old_level;
    pio->PIO_SODR = mask ^ old_level;
}


void pio_sync_write(Pio *pio, const uint32_t mask) {
    pio->PIO_ODSR = mask;
}


void pio_sync_toggle(Pio *pio, const uint32_t mask) {
    pio->PIO_ODSR = (pio->PIO_ODSR & mask) ^ mask;
}


uint32_t pio_read(Pio *pio, uint32_t mask) {
    return pio->PIO_PDSR & mask;
}


void pio_enable_interrupt(Pio *pio, const uint32_t mask) {
    pio->PIO_IER = mask;
}


void pio_disable_interrupt(Pio *pio, const uint32_t mask) {
    pio->PIO_IDR = mask;
}


void pio_configure_interrupt(Pio *pio, const uint32_t mask, const uint32_t attributes) {
    if (attributes & PIO_IT_AIME) {
        pio->PIO_AIMER = mask;

        if (attributes & PIO_IT_RE_OR_HL) {
            pio->PIO_REHLSR = mask;
        }
        else {
            pio->PIO_FELLSR = mask;
        }

        if (attributes & PIO_IT_EDGE) {
            pio->PIO_ESR = mask;
        }
        else {
            pio->PIO_LSR = mask;
        }
    }
    else {
        pio->PIO_AIMDR = mask;
    }
}
