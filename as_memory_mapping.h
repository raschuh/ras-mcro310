/**********************************************************************************
 *
 * as_memory_mapping.h
 *
 *
 * Robert Schuh
 * Created on 13 November 2017
 * Last Modified on --
 *
 **********************************************************************************/

#ifndef RAS_MEMORYMAP_H
#define RAS_MEMORYMAP_H

#include "armshoe.h"

#define LED1_PIN    (18)
#define LED2_PIN    (17)
#define B1_PIN      (19)
#define B2_PIN      (20)

#define LED1_MASK   (1 << LED1_PIN)
#define LED2_MASK   (1 << LED2_PIN)
#define B1_MASK     (1 << B1_PIN)
#define B2_MASK     (1 << B2_PIN)

#endif