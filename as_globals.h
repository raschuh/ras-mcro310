/**********************************************************************************
 *
 * as_globals.h
 *
 * Global variables such as USART0 RX buffer are declared here.
 *
 * Robert Schuh
 * Created on 6 October 2017
 * Last Modified on ---
 *
 **********************************************************************************/

#ifndef RAS_SAM3S4B_GLOBALS_HOME_H
#define RAS_SAM3S4B_GLOBALS_HOME_H

#include <sam3s.h>

#define _CPU_CLK   (64000000U)  // Maximum CPU clock speed for the SAM3S4B MCU - 64 MHz
#define _BUF_LEN_LONG  (256)    // Maximum buffer size - 256 bytes/chars - USART0 receive buffer uses this
#define _BUF_LEN_SHORT (128)    // Shorter buffer size

typedef struct {
    volatile char buffer[_BUF_LEN_LONG];
    volatile int  count;
} T_BUFFER;

extern T_BUFFER g_us0_rx;

extern int g_pdc_buffer_transferred;

typedef struct {
     uint32_t in_mask;
     uint8_t isr_flag;
     char msg[_BUF_LEN_SHORT];
} T_IN_MSG_MAP;

extern T_IN_MSG_MAP g_btn_msg_map[2];

typedef struct {
    short keypad;
    short parallel_data;
    short pio_msg;
} T_APP_MODE;

extern T_APP_MODE g_app_mode;

extern uint32_t g_par_data_in_mask;
extern uint32_t g_par_data_out_mask;
extern uint32_t g_par_in_clk_mask;
extern uint32_t g_par_out_clk_mask;
extern uint32_t g_par_rx_int_flag;
extern unsigned int g_par_rx_data;

extern T_BUFFER g_parallel_rx;

#endif