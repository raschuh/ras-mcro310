/**********************************************************************************
 *
 * as_functions.c
 *
 * Robert Schuh
 * Created on 7 October 2017
 * Last Modified on ---
 *
 **********************************************************************************/

#include "armshoe.h"

extern inline void app_init() {
    g_us0_rx.count = 0;   // Ready the USART0 buffer
    usart0_config();           // Set up USART0
    //__asm volatile("cpsie i"); // Enable interrupts globally
    __enable_irq();
}


int return_to_main() {
    // Check if the buffer is not empty and the last char is the ASCII escape char
    return ((g_us0_rx.count > 0) && (g_us0_rx.buffer[g_us0_rx.count - 1] == 27)) ? 1 : 0;
}


extern inline void delay(int timer) {
    while (timer--);
}


void hello() {
    char *msg = "\n\n\r\t\tHello beautiful universe!!\n\n\r";
    usart0_write(msg, strlen(msg));
    return;
}


void drNo() {
    char *msg = "\n\n\r\t\tDoktor No sez NO! WRONG! Try again? Weddy gud.\n\n\r";
    usart0_write(msg, strlen(msg));
    clear_us0_buffer();
    return;
}


void boop() {
    char msg[50];
    int  last_count = 0;
    clear_us0_buffer();

    snprintf(msg, 50, "\n\r\t\t----- Booper -----\r\n");
    usart0_write(msg, strlen(msg));

    while (1) {
        if (g_us0_rx.count == last_count) {
            continue;
        }

        if (return_to_main()) {
            break;
        }

        snprintf(msg, 50, "\t\tYou booped me with %c! Count: %d\r\n", g_us0_rx.buffer[g_us0_rx.count - 1], g_us0_rx.count);
        usart0_write(msg, strlen(msg));
        last_count = g_us0_rx.count;
    }

    clear_us0_buffer();
    return;
}


void blinky() {
    clear_us0_buffer();
    char msg[_BUF_LEN_LONG];
    uint32_t leds = LED1_MASK | LED2_MASK;

    snprintf(msg, _BUF_LEN_LONG, "\n\n\r\t\t---- BLINK BLINK ----\n\n\r\t\tPress ESC to end\n\n\r");
    usart0_write(msg, strlen(msg));

    pio_set_output(PIOA, leds, 1, PIO_DEFAULT);

    while (1) {
        if (return_to_main()) {
            break;
        }

        pio_toggle(PIOA, leds);
        delay(0xFFFFF);
    }

    clear_us0_buffer();

    return;
}


char ascii_state_map_test(char buf[], int buf_len, char map[][128])
{
    int state = 0;

    for (int i = 0; i < buf_len; i++) {
        state = map[state][buf[i]];

        if (state == 0) break;
    }

    return (state == 0) ? 0 : 1;
}